import os
import csv
from datetime import datetime
from tqdm import tqdm
from time import sleep
import pandas as pd

from pandas import read_csv as panda_csv
from pandas import datetime as panda_date

import matplotlib.pyplot as plt

import numpy as np


class Nodes:
    def __init__(self, alias):
        self._alias = alias
        self.__csv_data = []
        self.__csv_timestamps = []
        self.__csv_tid = None
        self.__sampled_data = None
        self.tid_gradient = 1
        self.heh_gradient = 1

        self.__initial_current = 0

    def get_alias(self):
        return self._alias

    def set_initial_current(self, current):
        self.__initial_current = current

    def append_csv_data(self, data):
        self.__csv_data.append(data)

    def append_csv_timestamp(self, data):
        self.__csv_timestamps.append(data)

    def set_csv_tid(self, data):
        self.__csv_tid = data

    def print_csv_data(self):
        print self.__csv_data

    def adjust_currents(self):
        # Adjusting the initial currents:
        # Comparing the first value to the initial value and find the offset
        first_measurement = self.__csv_data[0]
        current_offset = first_measurement - self.__initial_current

        tmp_data = []
        tmp_timestamp = []

        # Apply the offset to the data
        for index in range(len(self.__csv_data)):
            adjusted_current = self.__csv_data[index] - current_offset
            if adjusted_current >= 0:
                tmp_data.append(adjusted_current)
                tmp_timestamp.append(self.__csv_timestamps[index])

        self.__csv_data = tmp_data
        self.__csv_timestamps = tmp_timestamp

    def resample_data(self, noise=False):
        if self.__csv_data:
            # Upsample plot series with 5S frequency
            sample_series = pd.Series(self.__csv_data, self.__csv_timestamps, name='Currents')

            # Sort the series by index - in case that log files were not in correct order
            sample_series.sort_index(inplace=True)

            # Method to add synthetic noise to the signal - in case that there was missing data and interpolation
            # Looks a little too clean...
            if noise:
                timestamp_string = '2018-08-02 17:00:00'
                timestamp = pd.to_datetime(sample_series.index.values[0])
                limit_ts = pd.to_datetime(timestamp_string)
                index = None

                # Specify this interval: here it's from start till given timestamp
                if timestamp < limit_ts:
                    ts_series = pd.to_datetime(sample_series.index.values)
                    for i, values in enumerate(ts_series):
                        if values > limit_ts:
                            index = i
                            break
                    if not index:
                        raise ValueError

                    # Index for resampling is found and the signal will now be interpolated during the interval
                    upsampled_first = sample_series[0:index].resample('5S').last()
                    interpolated = upsampled_first.interpolate('pchip')

                    # Generating noise
                    for i, items in enumerate(upsampled_first):
                        if np.isnan(items):
                            noise = np.random.randint(-1, 2)
                            interpolated[i] += noise
                        else:
                            pass

                    # Adding the clean upsampled signal to the list
                    upsampled_last = sample_series[index:].resample('5S').bfill()
                    signal = interpolated.append(upsampled_last)
                else:
                    signal = sample_series.resample('5S').bfill()
            else:
                # .last() is not interpolated. Change it to bfill() if interpolation is wished
                signal = sample_series.resample('5S').last()

            # Combine the two resampled series
            combined = pd.concat([self.__csv_tid['TID'] * self.tid_gradient, self.__csv_tid['HEH'] * self.heh_gradient,
                                  signal], axis=1, names=['TID', 'HEH', 'Currents'])

            # Remove any NaN values
            self.__sampled_data = combined.dropna()

            # If there was current data from before the TID reset, we remove them manually, as we don't care about
            # this data.
            if not self.__sampled_data['TID'][0] == 0:
                zero_index = 0
                for index in range(len(self.__sampled_data['TID'])):
                    if not self.__sampled_data['TID'][index] == 0:
                        zero_index += 1
                    else:
                        self.__sampled_data = self.__sampled_data[zero_index:]
                        break

    def save_csv_data(self, save_dir):
        self.adjust_currents()
        self.resample_data()
        if self.__csv_data:
            self.__sampled_data.to_csv(save_dir + str(self._alias) + '.csv')


class SeparateLogs:
    def __init__(self):
        self.alias_dir = 'Aliases/'
        self.save_dir = 'formatLogs/'

        self.board_alias = []
        self.BoardObjs = []

        print '\nSeparate CAEN Logs:'
        print 'Finding aliases....'
        self.find_board_aliases()
        print 'Creating node objects....'
        self.create_node_objects()
        print 'Assigning initial currents....'
        self.find_initial_currents()
        print 'Analysing gradients....'
        self.find_gradients()
        print '\nAnalyzing files: \n'
        self.separate_all_logs()

    def separate_all_logs(self):
        print 'CAEN_Log_early_alias.csv ...'
        self.separate_csv_file('LogFiles/CAEN_Log_early_alias.csv')
        print 'caen_log_week1.txt ...'
        self.separate_txt_file('LogFiles/caen_log_week1.txt')
        print 'caen_log_week2.txt ...'
        self.separate_txt_file('LogFiles/caen_log_week2.txt')
        print 'caen_log_week3.txt ...'
        self.separate_txt_file('LogFiles/caen_log_week3.txt')
        print 'TID logs...'
        self.extract_tid()
        print '\n'
        self.save_all_csv_files()

    def create_node_objects(self):
        # Generate objects from the alias files
        for boards in self.board_alias:
            boardObj = Nodes(boards)
            self.BoardObjs.append(boardObj)

    def save_all_csv_files(self):
        # Function to save all boards' data as CSV
        for items in tqdm(self.BoardObjs, desc='Saving .csv data', ascii=True):
            items.save_csv_data(self.save_dir)

    def find_board_aliases(self):
        # Generates a list to identify the boards aliases from the alias files.
        alias_files = os.listdir(self.alias_dir)
        boards = []
        for aliases in alias_files:
            with open(self.alias_dir + aliases, 'rb') as f:
                reader = csv.DictReader(f)
                for row in reader:
                    boards.append(str(row['Alias']))
        # Creates a list with the alias names
        self.board_alias = list(set(boards))

        # Remove empty aliases
        self.board_alias.remove('')

    def find_gradients(self):
        # In case of flux gradient during radiation test
        file = 'Flux/Flux_gradient_factors.csv'
        gradients = []

        # Open the file and append the gradients to a list
        with open(file, 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                gradients.append(row)

        # Add the gradient to the board objects
        for boards in self.BoardObjs:
            alias = str(boards.get_alias()).strip()
            nodeid = alias[alias.find('_')+1: alias.find('_')+3]
            for row in gradients:
                row_nodeid = row[0][row[0].find('_')+1: row[0].find('_')+3]
                if nodeid == row_nodeid:
                    boards.tid_gradient = float(row[1])
                    boards.heh_gradient = float(row[2])

    def separate_txt_file(self, filename):
        # Function to separate the txt file to each board objects
        with open(filename, 'rb') as in_file:
            for line in in_file:
                # Finding IMon values (Monitored current)
                if 'IMon' in line and len(line) > 100:
                    # Extracting the value between 2526.xxxxxxx.IMon (the alias)
                    alias = line[line.find('2527.') + 5: line.find('.IMon')]
                    # Extracting the IMon value, as it's before the ts_source
                    imon_value = float(line[line.find('ts_source') - 8: line.find('ts_source') - 1]) * 1000
                    # Extracting timestamp
                    timestamp = line[-25:-2]
                    # Convert timestamp to datetime object
                    date = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f')

                    # Add data to corresponding Node Object
                    if alias:
                        index = self.alias_to_index(alias)
                        self.BoardObjs[index].append_csv_data(imon_value)
                        self.BoardObjs[index].append_csv_timestamp(date)

    # Only used for ELMB2 first file, as it was a different format...
    def separate_csv_file(self, filename):
        with open(filename, 'rb') as f:
            reader = csv.DictReader(f)
            fieldnames = reader.fieldnames
            boards = list(fieldnames)
            boards.remove('Timestamp')

            for row in reader:
                csv_ts = row['Timestamp']
                date = datetime.strptime(csv_ts, '%Y-%m-%dT%H:%M:%S.%fZ')

                for items in boards:
                    imon_value = float(row[str(items)]) * 1000
                    # Add data to corresponding Node Object
                    index = self.alias_to_index(items)
                    self.BoardObjs[index].append_csv_data(imon_value)
                    self.BoardObjs[index].append_csv_timestamp(date)

    # Add information from TID files
    def extract_tid(self, include_unformatted=True):
        formatted_tid = self.resample_TID('LogFiles/TID.csv')
        unformatted_tid = self.resample_TID('LogFiles/TID_unformatted.csv')

        for boards in self.BoardObjs:
            board_id = boards.get_alias()

            if '41' in board_id or '42' in board_id or '53' in board_id or '61' in board_id:
                boards.set_csv_tid(unformatted_tid)
            else:
                boards.set_csv_tid(formatted_tid)

    def resample_TID(self, file):
        # Open TID file as Pandas Series
        series = panda_csv(file, header=0, parse_dates=[0], index_col=0, squeeze=True, date_parser=self.timestamp_parser)

        # Resample and interpolate every5 seconds
        upsampled = series.resample('5S').bfill()
        return upsampled

    def timestamp_parser(self, x):
        return panda_date.strptime(x, '%d/%m/%Y %H:%M:%S')

    # HARD CODED VALUES WILL DIFFER FROM TESTS
    def find_initial_currents(self):
        for boards in self.BoardObjs:
            alias = boards.get_alias()
            if 'ELMB128' in alias:
                if 'ANALOG' in alias:
                    boards.set_initial_current(6)
                elif 'DIGITAL' in alias:
                    boards.set_initial_current(10)
                elif 'CAN' in alias:
                    boards.set_initial_current(20)

            elif 'ELMB2' in alias or 'AT90' in alias:
                if 'direct' in alias:
                    if 'ANALOG' in alias:
                        boards.set_initial_current(8)
                    elif 'DIGITAL' in alias:
                        boards.set_initial_current(8)
                    elif 'CAN' in alias:
                        boards.set_initial_current(12)
                else:
                    if 'ANALOG' in alias:
                        boards.set_initial_current(12)
                    elif 'DIGITAL' in alias:
                        boards.set_initial_current(8)
                    elif 'CAN' in alias:
                        boards.set_initial_current(16)

    def alias_to_index(self, alias):
        return self.board_alias.index(alias)


if __name__ == '__main__':
    SeparateLogs()