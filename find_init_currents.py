import os
import csv

file_dir = 'LogFiles/'
alias_dir = 'Aliases/'


def find_board_aliases(week):
    boards = []
    with open(alias_dir + 'alias' + str(week) + '.csv', 'rb') as f:
        reader = csv.DictReader(f)
        for row in reader:
            boards.append(str(row['Alias']))
    try:
        boards.remove('')
    except ValueError:
        print 'No empty boards in list.'
    return boards


def find_init_currents(week, csv_writer):
    filename = file_dir + 'caen_log_week' + str(week) + '.txt'
    boards_alias = find_board_aliases(week)
    max_retries = 50000

    print boards_alias

    for boards in boards_alias:
        new_board = True
        with open(filename, 'rb') as in_file:
            retries = 0

            for line in in_file:
                retries += 1
                if str(boards) + '.IMon' in line and len(line) > 100 and new_board:
                    imon = float(line[line.find('ts_source') - 6: line.find('ts_source')])
                    if boards:
                        csv_writer.writerow({'Board Alias': str(boards), 'Initial Current': str(imon)})
                        print boards, imon
                    new_board = False
                if retries == max_retries:
                    break


if __name__ == '__main__':
    for i in range(1, 4):
        with open('InitialCurrents Week' + str(i) + '.csv', 'w') as csvfile:
            fieldnames = ['Board Alias', 'Initial Current']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            find_init_currents(i, writer)
