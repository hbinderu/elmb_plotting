import csv
import sys
import os
from time import sleep


class BoardIdToAlias:
    boards_filename = 'caen_log_15082018_1031.txt'  # Just placeholder from earlier test
    file_dir = 'caen_logs_formatted'
    alias_filename = 'Aliases/alias3.csv'           # Placeholder as well

    def __init__(self):
        self.boards = []
        self.boards_alias = []

    def convert(self, filename):
        self.boards_filename = filename
        self.alias_filename = 'Aliases/' + filename.replace('.txt', '.csv')

        self.find_board_and_channels()
        self.find_board_aliases()

        if not len(self.boards) or len(self.boards_alias):
            print('No Board IDs or Aliases found..')
            return

        if not os.path.isdir(self.file_dir):
            os.mkdir(self.file_dir)

        print('heheheh')
        sleep(10000)

        self.rename_txt_file()

    def rename_txt_file(self):
        with open(self.boards_filename, 'r') as r_file:
            filedata = r_file.read()

        # for index in range(0, len(self.boards)):
        #     msg = 'Renaming ' + self.boards[index] + ' to ' + self.boards_alias[index]
        #     print(msg)
        #     filedata = filedata.replace(str(self.boards[index]), str(self.boards_alias[index]))

        for index, boards in enumerate(self.boards):
            msg = 'Renaming ' + boards + ' to ' + self.boards_alias[index]
            print(msg)
            filedata = filedata.replace(str(boards), str(self.boards_alias[index]))

        boards_new_filename = self.file_dir + '/' + self.boards_filename
        with open(boards_new_filename, 'w') as w_file:
            w_file.write(filedata)

    def find_board_aliases(self):
        with open(self.alias_filename, 'rb') as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.boards_alias.append(str(row['Alias']))

    def find_board_and_channels(self):
        with open(self.alias_filename, 'rb') as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.boards.append(str(row['BoardId']))

    def find_board_and_channels_from_file(self):
        max_tries = 1000
        for board in range(0, 10):
            for channel in range(0, 10):
                id_name = 'Board0' + str(board) + '.Chan00' + str(channel)
                with open(self.boards_filename) as in_file:
                    amount_tries = 0
                    for line in in_file:
                        amount_tries += 1
                        if id_name in line:
                            self.boards.append(id_name)
                            break
                        elif amount_tries == max_tries:
                            break
