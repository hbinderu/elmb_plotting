import matplotlib
matplotlib.use('Qt4Agg')

import sys
import os
import random
import time
from PyQt4 import QtCore, QtGui, uic
import matplotlib.pyplot as plt
from datetime import datetime
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from openpyxl import load_workbook

import pandas as pd
from pandas import read_csv as panda_csv
import numpy as np

import csv
import warnings

warnings.filterwarnings("ignore", message="axes.hold is deprecated")
warnings.filterwarnings("ignore", message="Adding an axes using the same arguments as a previous axes currently "
                                          "reuses the earlier instance")

plot_id = 111

qtCreatorFile = "lib/main.ui" # my Qt Designer file

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class InteractivePlots(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.addToolBar(NavigationToolbar(self.canvas, self))
        self.pushButton_generate.clicked.connect(self.generate_button)
        self.pushButton_scan.clicked.connect(self.scan_button)
        self.pushButton_clear.clicked.connect(self.clear_button)
        self.lineEdit_search.textChanged.connect(self.search)
        self.comboBox_node.currentIndexChanged.connect(self.node_changed)

        self.boards = []
        self.boards_alias = []
        self.line_lengths = []

        self.log_dir = 'LogFiles/'
        self.alias_dir = 'Aliases/'
        self.adc_dir = '/home/CanDumpAnalysis/SeparatedADC/'
        self.power_events_file = 'bin/PowerEvents.xlsx'
        self.twinax = None

        self.setup()

    def check_os(self):
        if os.name == 'nt':
            self.log_dir = 'LogFiles/'
            self.alias_dir = 'Aliases/'
            self.adc_dir = 'SeparatedADC/'

    def setup(self):
        # Enable twin axes on the figure
        ax = self.canvas.figure.add_subplot(plot_id)
        self.twinax = ax.twinx()

        self.check_os()

    def clear_button(self):
        self.canvas.figure.clf()
        self.canvas.figure.add_subplot(plot_id)
        self.canvas.draw()

        # Reenable the twin axes on the new figure
        ax = self.canvas.figure.add_subplot(plot_id)
        self.twinax = ax.twinx()

    def search(self):
        self.comboBox_s1.clear()
        search_text = str(self.lineEdit_search.text())

        for alias in self.boards_alias:
            add_item = True

            # Check all items separated by space
            for items in search_text.split():
                if not items.lower() in alias.lower():
                    add_item = False
                    break

            # Only if all items are in the string - add to the comboBox_s1
            if add_item and len(alias):
                self.comboBox_s1.addItem(alias)

    def node_changed(self):
        self.comboBox_adc_channel.clear()

        node = self.comboBox_node.currentText()
        channel_dir = self.adc_dir + str(node) + '/'
        files = os.listdir(channel_dir)
        combobox_items = []

        for items in files:
            combobox_items.append(items[:-4])

        self.comboBox_adc_channel.addItems(combobox_items)

    def alias_to_index(self, alias):
        return self.boards_alias.index(alias)

    def generate_button(self):
        self.pushButton_generate.setEnabled(False)
        self.pushButton_generate.setText('Wait...')
        app.processEvents()

        alias = self.comboBox_s1.currentText()

        # Plot the board
        board_index = self.alias_to_index(alias)
        self.current_plot(self.boards_alias[board_index])

        # Plot ADC if checked
        try:
            if self.checkBox_adc.isChecked():
                if not self.checkBox_hold.isChecked():
                    ax = self.canvas.figure.add_subplot(plot_id)
                    self.twinax = ax.twinx()
                node = self.comboBox_node.currentText()
                channel = self.comboBox_adc_channel.currentText()
                self.ADC_plot(node, channel)
            if self.checkBox_powerevents.isChecked():
                self.plot_power_events(alias)

            self.canvas.draw()
        except ValueError:
            print "You can't combine TID with datetime on one axis"

        self.pushButton_generate.setEnabled(True)
        self.pushButton_generate.setText('Generate Plot')
        app.processEvents()

    def scan_button(self):
        self.boards = []
        self.boards_alias = []
        self.comboBox_s1.clear()
        self.comboBox_node.clear()
        self.find_board_aliases()
        self.add_adc_to_combobox()

        if len(self.boards_alias):

            # Remove duplictes and sort the list before adding to comboBox
            self.boards_alias = list(set(self.boards_alias))
            self.boards_alias.sort()

            # Remove search string:
            self.lineEdit_search.clear()

            # Add aliases to combobox
            self.comboBox_s1.addItems(self.boards_alias)

            # Enable interface
            self.pushButton_generate.setEnabled(True)
            self.pushButton_clear.setEnabled(True)

            # Allows adding empty aliases to the list
            # This procedure removes the empty element from the combobox, as there's no empty alias in the log files

            if not len(self.comboBox_s1.currentText()):
                self.comboBox_s1.removeItem(0)

            self.comboBox_s1.setEnabled(True)
            self.checkBox_hold.setEnabled(True)
            self.comboBox_node.setEnabled(True)
            self.checkBox_adc.setEnabled(True)
            self.checkBox_mask.setEnabled(True)
            self.comboBox_filter.setEnabled(True)
            self.checkBox_format.setEnabled(True)
            self.checkBox_trend.setEnabled(True)
            self.checkBox_powerevents.setEnabled(True)
            self.spinBox_order.setEnabled(True)
            self.spinBox_limit_min.setEnabled(True)
            self.spinBox_limit_max.setEnabled(True)
            self.spinBox_trendline.setEnabled(True)

    def find_board_aliases(self):
        alias_files = os.listdir(self.alias_dir)

        for aliases in alias_files:
            with open(self.alias_dir + aliases, 'rb') as f:
                reader = csv.DictReader(f)
                for row in reader:
                    self.boards_alias.append(str(row['Alias']))

    def extract_adc_data(self, node, channel):
        file_dir = self.adc_dir + str(node) + '/' + channel + '.csv'
        with open(file_dir, 'r') as csvfile:
            reader = csv.reader(csvfile)
            timestamp = []
            data = []
            for row in reader:

                # Checking for uni or bi-polar adc settings; adjusting for signed / unsigned integers from server
                if not int(row[2], 2) & 1:
                    if int(row[1]) > 32768:
                        real_data = int(row[1]) - 65536
                    else:
                        real_data = int(row[1])
                else:
                    real_data = int(row[1])

                # Check if overvoltage mask is enabled (bit 7):
                if not (self.checkBox_mask.isChecked() and int(row[2], 2) & 128):
                    data.append(real_data)
                    timestamp.append(datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S.%f'))

        return timestamp, data

    def add_adc_to_combobox(self):
        folders = os.listdir(self.adc_dir)
        nodes = []
        for items in folders:
            nodes.append(items)

        nodes.sort()
        self.comboBox_node.addItems(nodes)

    def ADC_plot(self, node, channel):
        ax = self.twinax
        title = 'Node: ', str(node), 'Chann: ', str(channel)

        # Check if the signal is already plotted
        handles, labels = ax.get_legend_handles_labels()
        for label in labels:
            if str(title) in label:
                # Signal is already plotted
                return

        x, y = self.extract_adc_data(node, channel)

        ax.plot(x, y, '-', label=str(title))
        ax.set(xlabel='Server Timestamp', ylabel='ADC Bit Count')
        self.canvas.figure.autofmt_xdate()
        ax.legend(loc=2)

    def current_plot(self, board_id):
        # Clear figure if hold is not checked
        if not self.checkBox_hold.checkState():
            self.canvas.figure.clf()

        # Check if limit values are set correctly
        limit_min = self.spinBox_limit_min.value()
        limit_max = self.spinBox_limit_max.value()
        if limit_min and limit_max:
            if limit_min > limit_max:
                print 'You just filtered out all data..... :^)'
                return

        if self.checkBox_format.isChecked():
            format_dir = 'formatLogsNoise/'
        else:
            format_dir = 'formatLogs/'

        # Create subplot object
        ax = self.canvas.figure.add_subplot(plot_id)

        # Check if the signal is already plotted
        handles, labels = ax.get_legend_handles_labels()
        for label in labels:
            if board_id in label:
                # Signal is already plotted
                return

        # Extract data from log files
        csvfile = format_dir + str(board_id) + '.csv'
        series = panda_csv(csvfile, parse_dates=[0], index_col=0, squeeze=True)

        if self.comboBox_filter.currentText() == 'Averaging':
            order = self.spinBox_order.value()
            current_series = series['Currents'].rolling(window=order).mean()
        else:
            current_series = series['Currents']

        if limit_min or limit_max:
            if limit_max:
                current_series = current_series[(limit_min <= current_series) & (current_series <= limit_max)]
            elif limit_min:
                current_series = current_series[limit_min <= current_series]

            series = pd.concat([current_series, series['TID'], series['HEH']], axis=1, names=['Currents', 'TID', 'HEH'])
            series.dropna(inplace=True)

        if self.comboBox_type.currentIndex() == 1:   # Check box for TID
            plot_x = series['TID']
            plot_y = current_series.values
            x_label = 'TID [krad]'
            y_label = 'Current [mA]'

        elif self.comboBox_type.currentIndex() == 2:
            plot_x = series['HEH']
            plot_y = current_series.values
            x_label = 'HEHeq Fluence [cm-2]'
            y_label = 'Current [mA]'

        else:
            plot_x = current_series.index
            plot_y = current_series.values
            x_label = 'Server Timestamp'
            y_label = 'Current [mA]'

        signal = pd.Series(plot_y, index=plot_x)

        trendline_value = self.spinBox_trendline.value()
        if trendline_value:
            x = np.arange(len(plot_x))
            fit = np.polyfit(x, plot_y, trendline_value)
            fit_fn = np.poly1d(fit)

            if self.checkBox_trend.isChecked():
                trendline = pd.Series(fit_fn(x), index=plot_x)
                trendline.sort_index(inplace=True)
                ax.plot(trendline, label=str(board_id) + ' - Trendline')  # plot trendline first
            else:
                signal = pd.Series(fit_fn(x), index=plot_x)

        signal.sort_index(inplace=True)
        signal = signal[~signal.index.duplicated(keep='last')]
        ax.plot(signal, label=str(board_id))  # plotting the assigned series
        ax.set(xlabel=x_label, ylabel=y_label)

        # Auto format figure for using datetime objects as x-axis
        self.canvas.figure.autofmt_xdate()

        # Place legend at optimal place, format x-axis and redraw the figure
        ax.legend(loc=1)

    def plot_power_events(self, alias):
        node_id = alias[str(alias).find('_')+1:str(alias).find('_')+3]
        status, msg, timestamp = self.find_xlsx_interval(alias)

        wb = load_workbook(self.power_events_file, read_only=True)
        ws = wb[node_id]

        power_events_on = []
        power_events_off = []
        last_event = None

        for rows in ws.rows:
            try:
                if 'Status' in rows[status].value:
                    if 'OFF' in rows[msg].value and not last_event == 'OFF':
                        power_events_off.append(rows[timestamp].value)
                        last_event = 'OFF'
                    elif 'ON' in rows[msg].value and last_event == 'OFF':
                        power_events_on.append(rows[timestamp].value)
                        last_event = 'ON'
            except (IndexError, TypeError):
                break

        if power_events_on:
            for items in power_events_on:
                x_point = datetime.strptime(items, '%Y-%m-%d %H:%M:%S.%f')
                self.canvas.plt.axvline(x=x_point, color='g')

        if power_events_off:
            for items in power_events_off:
                x_point = datetime.strptime(items, '%Y-%m-%d %H:%M:%S.%f')
                self.canvas.plt.axvline(x=x_point, color='r')

    def find_xlsx_interval(self, alias):
        if 'ANALOG' in alias:
            return 0, 1, 2
        elif 'DIGITAL' in alias:
            return 4, 5, 6
        elif 'CAN' in alias:
            return 8, 9, 10
        else:
            raise ValueError('No power type was found in alias')

    def plot_vline(self, x_point):
        self.canvas.plt.axvline(x=x_point)


if __name__ == "__main__":
    global app  # Enables GUI refresh from interface class
    app = QtGui.QApplication(sys.argv)
    window = InteractivePlots()
    window.show()
    sys.exit(app.exec_())